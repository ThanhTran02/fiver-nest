import { Controller, Get, Post, Body, Param, Delete, Req, Res, Put, UseGuards } from '@nestjs/common';
import { TypeJobService } from './type-job.service';
import { CreateTypeJobDto } from './dto/create-type-job.dto';
import { UpdateTypeJobDto } from './dto/update-type-job.dto';
import {Request,Response} from "express"
import{ApiTags,ApiBearerAuth, ApiBody} from "@nestjs/swagger"
import { JwtAuthGuard } from 'src/auth/auth.guard';

@ApiTags("LoaiCongViec")
@ApiBearerAuth()
@Controller('api/v1/loai-cong-viec')
export class TypeJobController {
  constructor(private readonly typeJobService: TypeJobService) {}

  @Post()
  @ApiBody({type:CreateTypeJobDto})
  async create(@Body() data: any ,@Res() res:Response):Promise<any> {
    await this.typeJobService.create(data);
    return res.status(200).json({
      status:"200",
      message:"Successfully!",
      content:""
    })
  }

  @Get()
  async findAll(@Res() response:Response):Promise<any> {
    const result = await this.typeJobService.findAll();
     return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get(':id')
  async findOne(@Param('id') id: string,@Res()response:Response):Promise<any> {
    const result = await this.typeJobService.findOne(+id);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @UseGuards(JwtAuthGuard)
  @ApiBody({type:CreateTypeJobDto})
  @Put(':id')
  async update(@Param('id') id: string, @Body() data: any,@Res() res:Response):Promise<any> {
    await this.typeJobService.update(+id, data);
    return res.status(200).json({
      status:"200",
      message:"Successfully!",
      content:""
    })
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string,@Res() res:Response):Promise<any> {
    await this.typeJobService.remove(+id);
    return res.status(200).json({
      status:"200",
      message:"Successfully!",
      content:""
    })
  }
}
