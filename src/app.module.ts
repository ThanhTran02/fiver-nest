import { Module } from '@nestjs/common';

import { CommentModule } from './comment/comment.module';
import { HireJobModule } from './hire-job/hire-job.module';
import { UserModule } from './user/user.module';
import { TypeJobModule } from './type-job/type-job.module';
import { DetailJobModule } from './detail-job/detail-job.module';
import { JobModule } from './job/job.module';
import { AuthModule } from './auth/auth.module';


@Module({
  imports: [AuthModule,CommentModule, DetailJobModule, JobModule, TypeJobModule, UserModule, HireJobModule,JobModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
