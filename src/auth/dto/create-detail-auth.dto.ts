import { ApiProperty } from "@nestjs/swagger";

export class CreateAuthDto {
    @ApiProperty()
    name: string;
    @ApiProperty()
    email: string;
}

export class CreateDetailAuthDto {
    @ApiProperty()
    name: string;
    @ApiProperty()
    email: string;
    @ApiProperty()
    pass_word: String
    @ApiProperty()
    phone: String
    @ApiProperty()
    birth_day: String
    @ApiProperty()
    gender: String
    @ApiProperty()
    role: String
    @ApiProperty()
    skill: String
    @ApiProperty()
    certification: String
}
