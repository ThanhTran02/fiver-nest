import { Injectable, NotFoundException, UnprocessableEntityException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma.service';
import * as bcrypt from 'bcrypt';
@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService,
        private readonly prisma: PrismaService,
    ) {}


    async login(body:any):Promise<any> {
        const { email , pass_word } = body
        const user = await this.prisma.nguoiDung.findFirst({where:{email}})
        
        if (!user) {
            throw new NotFoundException('User not found!');
        }

        if (!await bcrypt.compare(pass_word, user.pass_word)) {
            throw new NotFoundException('Invalid password!')
        }

        return {token:this.jwtService.sign({data:{user}})}
    }

    async signUp(body:any):Promise<any> {
        const { email, pass_word } = body
        const findEmail = await this.prisma.nguoiDung.findFirst({where:{email:email,exist:false}})
        if(findEmail){
            throw new UnprocessableEntityException(`Email ${email} already exists!`)
        }
        else{
            const hashedPassword = await bcrypt.hash(pass_word,10)
            return this.prisma.nguoiDung.create({data:{...body,pass_word:hashedPassword}})
        }
        
    }   
}
