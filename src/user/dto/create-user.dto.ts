import { ApiProperty } from "@nestjs/swagger";

export class CreateUserDto {
    @ApiProperty()
    name: string;
    @ApiProperty()
    email: string;
    @ApiProperty()
    pass_word: String
    @ApiProperty()
    phone: String
    @ApiProperty()
    birth_day: String
    @ApiProperty()
    gender: String
    @ApiProperty()
    role: String
    @ApiProperty()
    skill: String
    @ApiProperty()
    certification: String
}
