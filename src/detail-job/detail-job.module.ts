import { Module } from '@nestjs/common';
import { DetailJobService } from './detail-job.service';
import { DetailJobController } from './detail-job.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [DetailJobController],
  providers: [DetailJobService,PrismaService],
})
export class DetailJobModule {}
