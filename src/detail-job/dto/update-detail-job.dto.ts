import { PartialType } from '@nestjs/mapped-types';
import { CreateDetailJobDto } from './create-detail-job.dto';

export class UpdateDetailJobDto extends PartialType(CreateDetailJobDto) {}
