import { ApiProperty } from "@nestjs/swagger"

export class CreateJobDto {
    @ApiProperty()
    ten_cong_viec: String
    @ApiProperty()
    danh_gia: Number
    @ApiProperty()
    gia_tien: Number
    @ApiProperty()
    hinh_anh: String
    @ApiProperty()
    mo_ta: String
    @ApiProperty()
    mo_ta_ngan: String
    @ApiProperty()
    sao_cong_viec: Number
    @ApiProperty()
    ma_chi_tiet_loai: Number
    @ApiProperty()
    nguoi_tao: Number
}
export class FileUploadDto {
    @ApiProperty({ type: 'string', format: 'binary' })
    file: any;
  }
