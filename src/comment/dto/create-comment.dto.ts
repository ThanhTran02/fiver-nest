import { ApiProperty } from "@nestjs/swagger"

export class CreateCommentDto {
    @ApiProperty()
    ma_cong_viec: Number
    @ApiProperty()
    ma_nguoi_binh_luan: Number
    @ApiProperty()
    ngay_binh_luan: Date
    @ApiProperty()
    noi_dung: String
    @ApiProperty()
    sao_binh_luan: Number
 }
