import { Controller, Get, Post, Body, Param, Delete, Req, Res, Put, UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import{Request,Response} from "express"
import { ApiTags,ApiBearerAuth, ApiBody } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/auth.guard';
import { CreateCommentDto } from './dto/create-comment.dto';

@ApiBearerAuth()
@ApiTags("BinhLuan")
@Controller('api/v1/binh-luan')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @ApiBody({type:CreateCommentDto})
  @Post()
  async create(@Body() data: any ,@Res() res:Response):Promise<any> {
    const result =await this.commentService.create(data);
    return res.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get()
  async findAll(@Res() response:Response):Promise<any> {
    const result = await this.commentService.findAll();
     return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get('lay-binh-luan-theo-cong-viec/:id')
  async findByJobID(@Param('id') id: string ,@Res() res:Response):Promise<any> {
    const result = await this.commentService.findByJobID(+id);
    return res.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }


  @ApiBody({type:CreateCommentDto})
  @Put(':id')
  async update(@Param('id') id: string, @Body() data: any,@Res() res:Response):Promise<any> {
    const result =await this.commentService.update(+id, data);
    return res.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Req() req,@Param('id') id: string,@Res() res:Response):Promise<any> {
    await this.commentService.remove(+id,req.user.id);
    return res.status(200).json({
      status:"200",
      message:"Successfully!",
      content:""
    })
  }
}
