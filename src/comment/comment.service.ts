import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';

const processDate = (data: any) => {
 
  const dateParts = data.split('/');
  const day = parseInt(dateParts[0], 10);
  const month = parseInt(dateParts[1], 10) - 1; 
  const year = parseInt(dateParts[2], 10); 
  const dateTime = new Date(year, month, day);
  return dateTime;
};

@Injectable()
export class CommentService {
  constructor(private readonly prisma:PrismaService){}
  create(data: any) {
    return this.prisma.binhLuan.create({data:{...data,ngay_binh_luan:processDate(data.ngay_binh_luan)}});
  }

  findAll() {
    return this.prisma.binhLuan.findMany({where:{exist:false}});
  }
  findByJobID(id:number){
    return this.prisma.binhLuan.findMany({where:{exist:false,ma_cong_viec:id},include:{NguoiDung:{
      select:{name:true}
    }}})
  }

  update(id: number, data:any) {
    return this.prisma.binhLuan.update({where:{id},data:{...data,ngay_binh_luan:processDate(data.ngay_binh_luan)}});
  }

  async remove(id: number,idUser:number) {   
    return this.prisma.binhLuan.update({where:{id,exist:false,ma_nguoi_binh_luan:idUser},data:{exist:true}});
  }
}
