import { Module } from '@nestjs/common';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { PrismaService } from 'src/prisma.service';
import { JwtAuthGuard } from 'src/auth/auth.guard';

@Module({
  controllers: [CommentController],
  providers: [CommentService,PrismaService,JwtAuthGuard],
})
export class CommentModule {}
