import { Module } from '@nestjs/common';
import { HireJobService } from './hire-job.service';
import { HireJobController } from './hire-job.controller';
import { PrismaService } from 'src/prisma.service';
import { jwtStrategy } from 'src/auth/jwt.strategy';

@Module({
  controllers: [HireJobController],
  providers: [HireJobService,PrismaService,jwtStrategy],
})
export class HireJobModule {}
