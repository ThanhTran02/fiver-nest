import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';

const processDate = (data: string) => {
  const dateParts = data.split('/');
  const day = parseInt(dateParts[0], 10)+1;
  const month = parseInt(dateParts[1], 10) - 1; 
  const year = parseInt(dateParts[2], 10);
  const dateTime = new Date(year, month, day);
  return dateTime;
};
@Injectable()
export class HireJobService {
  constructor(private readonly prisma: PrismaService) {}

  create(data: any) {
    return this.prisma.thueCongViec.create({ data:{...data,ngay_thue:processDate(data.ngay_thue)} });
  }

  async findAll() {
    return this.prisma.thueCongViec.findMany({ where: { exist: false } });
  }

  async findOne(id: number) {
    return this.prisma.thueCongViec.findUnique({ where: { id, exist: false } });
  }

  async findJobHired(id:Number){
    return this.prisma.thueCongViec.findMany({where:{ma_nguoi_thue:Number(id),exist:false},include:{NguoiDung:{select:{email:true,name:true}},CongViec:true}})
  }
  
  async update(id: number, data: any) {
    return this.prisma.thueCongViec.update({where:{id,exist:false},data:{...data,ngay_thue:processDate(data.ngay_thue)}});
  }
  async updateJobHired(id:number){
    const checkId = await this.prisma.thueCongViec.findUnique({where:{id}})
    if(!checkId) 
      throw new BadRequestException("No Found id!")
    return this.prisma.thueCongViec.update({where:{id,exist:false},data:{hoan_thanh:true}})
  }

  async remove(id: number) {
    const checkId = await this.prisma.thueCongViec.findUnique({where:{id}})
    if(!checkId) 
      throw new BadRequestException("No Found id!")
    return this.prisma.thueCongViec.update({where:{id,exist:false},data:{exist:true}});
  }
}
