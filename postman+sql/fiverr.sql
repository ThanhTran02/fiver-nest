/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `BinhLuan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ma_cong_viec` int DEFAULT NULL,
  `ma_nguoi_binh_luan` int DEFAULT NULL,
  `ngay_binh_luan` datetime DEFAULT NULL,
  `noi_dung` varchar(255) DEFAULT NULL,
  `sao_binh_luan` int DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_binhLuan_Cv` (`ma_cong_viec`),
  KEY `fk_binhLuan_ND` (`ma_nguoi_binh_luan`),
  CONSTRAINT `fk_binhLuan_Cv` FOREIGN KEY (`ma_cong_viec`) REFERENCES `CongViec` (`id`),
  CONSTRAINT `fk_binhLuan_ND` FOREIGN KEY (`ma_nguoi_binh_luan`) REFERENCES `NguoiDung` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ChiTietLoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_chi_tiet` varchar(255) DEFAULT NULL,
  `hinh_anh` varchar(255) DEFAULT NULL,
  `ma_loai_cong_viec` int DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_ChiTietLoaiCongViec_LoaiCv` (`ma_loai_cong_viec`),
  CONSTRAINT `fk_ChiTietLoaiCongViec_LoaiCv` FOREIGN KEY (`ma_loai_cong_viec`) REFERENCES `LoaiCongViec` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `CongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_cong_viec` varchar(255) DEFAULT NULL,
  `danh_gia` int DEFAULT NULL,
  `gia_tien` int DEFAULT NULL,
  `hinh_anh` varchar(255) DEFAULT NULL,
  `mo_ta` varchar(255) DEFAULT NULL,
  `mo_ta_ngan` varchar(255) DEFAULT NULL,
  `sao_cong_viec` int DEFAULT NULL,
  `ma_chi_tiet_loai` int DEFAULT NULL,
  `nguoi_tao` int DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_TenBang_TenKhac` (`nguoi_tao`),
  KEY `fk_CongViec_ChitietLoaiCv` (`ma_chi_tiet_loai`),
  CONSTRAINT `fk_CongViec_ChitietLoaiCv` FOREIGN KEY (`ma_chi_tiet_loai`) REFERENCES `ChiTietLoaiCongViec` (`id`),
  CONSTRAINT `fk_TenBang_TenKhac` FOREIGN KEY (`nguoi_tao`) REFERENCES `NguoiDung` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `LoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_loai_cong_viec` varchar(255) DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `NguoiDung` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pass_word` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `birth_day` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `skill` varchar(255) DEFAULT NULL,
  `certification` varchar(255) DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ThueCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ma_cong_viec` int DEFAULT NULL,
  `ma_nguoi_thue` int DEFAULT NULL,
  `ngay_thue` datetime DEFAULT NULL,
  `hoan_thanh` tinyint(1) DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_thuecv_ndd` (`ma_nguoi_thue`),
  KEY `fk_thuecv_cv` (`ma_cong_viec`),
  CONSTRAINT `fk_thuecv_cv` FOREIGN KEY (`ma_cong_viec`) REFERENCES `CongViec` (`id`),
  CONSTRAINT `fk_thuecv_ndd` FOREIGN KEY (`ma_nguoi_thue`) REFERENCES `NguoiDung` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `BinhLuan` (`id`, `ma_cong_viec`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`, `exist`) VALUES
(1, 1, 2, '2023-01-01 08:00:00', 'Bình luận 1 cho công việc 1', 4, 0);
INSERT INTO `BinhLuan` (`id`, `ma_cong_viec`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`, `exist`) VALUES
(2, 2, 3, '2023-02-02 10:30:00', 'Bình luận 2 cho công việc 2', 5, 0);
INSERT INTO `BinhLuan` (`id`, `ma_cong_viec`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`, `exist`) VALUES
(3, 3, 4, '2023-03-03 12:45:00', 'Bình luận 3 cho công việc 3', 3, 0);
INSERT INTO `BinhLuan` (`id`, `ma_cong_viec`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`, `exist`) VALUES
(4, 4, 5, '2023-04-04 15:15:00', 'Bình luận 4 cho công việc 4', 2, 0),
(5, 5, 1, '2023-05-05 18:00:00', 'Bình luận 5 cho công việc 5', 5, 0),
(7, 1, 2, '1908-09-11 16:53:30', 'string', 0, 1),
(12, 5, 1, '2023-12-01 17:00:00', 'string', 0, 0),
(13, 5, 6, '2023-12-01 17:00:00', 'good', 5, 1);

INSERT INTO `ChiTietLoaiCongViec` (`id`, `ten_chi_tiet`, `hinh_anh`, `ma_loai_cong_viec`, `exist`) VALUES
(1, 'Chi tiết 1', 'uploads\\1703167876212-398137397.png', 1, 0);
INSERT INTO `ChiTietLoaiCongViec` (`id`, `ten_chi_tiet`, `hinh_anh`, `ma_loai_cong_viec`, `exist`) VALUES
(2, 'Chi tiết 2', 'anh2.jpg', 2, 0);
INSERT INTO `ChiTietLoaiCongViec` (`id`, `ten_chi_tiet`, `hinh_anh`, `ma_loai_cong_viec`, `exist`) VALUES
(3, 'Chi tiết 3', 'anh3.jpg', 3, 0);
INSERT INTO `ChiTietLoaiCongViec` (`id`, `ten_chi_tiet`, `hinh_anh`, `ma_loai_cong_viec`, `exist`) VALUES
(4, 'Chi tiết 4', 'anh4.jpg', 4, 0),
(5, 'Chi tiết 5', 'anh5.jpg', 5, 0),
(7, 'Chi tiết 6', 'anh6.jpg', 1, 0),
(13, '13', '13.png', 1, 1);

INSERT INTO `CongViec` (`id`, `ten_cong_viec`, `danh_gia`, `gia_tien`, `hinh_anh`, `mo_ta`, `mo_ta_ngan`, `sao_cong_viec`, `ma_chi_tiet_loai`, `nguoi_tao`, `exist`) VALUES
(1, 'Công việc 1', 4, 100, 'uploads\\1703149273016-213188982.png', 'Mô tả công việc 1', 'Mô tả ngắn 1', 4, 1, 1, 0);
INSERT INTO `CongViec` (`id`, `ten_cong_viec`, `danh_gia`, `gia_tien`, `hinh_anh`, `mo_ta`, `mo_ta_ngan`, `sao_cong_viec`, `ma_chi_tiet_loai`, `nguoi_tao`, `exist`) VALUES
(2, 'Công việc 2', 5, 150, 'cv2.jpg', 'Mô tả công việc 2', 'Mô tả ngắn 2', 5, 2, 2, 0);
INSERT INTO `CongViec` (`id`, `ten_cong_viec`, `danh_gia`, `gia_tien`, `hinh_anh`, `mo_ta`, `mo_ta_ngan`, `sao_cong_viec`, `ma_chi_tiet_loai`, `nguoi_tao`, `exist`) VALUES
(3, '3', 5, 80, '1234', '1234', '#e1e7ed', 5, 1, 2, 0);
INSERT INTO `CongViec` (`id`, `ten_cong_viec`, `danh_gia`, `gia_tien`, `hinh_anh`, `mo_ta`, `mo_ta_ngan`, `sao_cong_viec`, `ma_chi_tiet_loai`, `nguoi_tao`, `exist`) VALUES
(4, 'Công việc 4', 2, 120, 'cv4.jpg', 'Mô tả công việc 4', 'Mô tả ngắn 4', 2, 4, 4, 0),
(5, 'Công việc 5', 5, 200, 'cv5.jpg', 'Mô tả công việc 5', 'Mô tả ngắn 5', 5, 5, 5, 0),
(14, 'string', 0, 99999, 'uploads\\1703300061487-120231029.png', 'dev', 'app', 5, 2, 1, 0);

INSERT INTO `LoaiCongViec` (`id`, `ten_loai_cong_viec`, `exist`) VALUES
(1, 'Làm vườn', 0);
INSERT INTO `LoaiCongViec` (`id`, `ten_loai_cong_viec`, `exist`) VALUES
(2, 'Lập trình', 0);
INSERT INTO `LoaiCongViec` (`id`, `ten_loai_cong_viec`, `exist`) VALUES
(3, 'Thiết kế đồ họa', 0);
INSERT INTO `LoaiCongViec` (`id`, `ten_loai_cong_viec`, `exist`) VALUES
(4, 'Viết nội dung', 0),
(5, 'Chăm sóc trẻ', 0),
(6, 'qc', 0);

INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `skill`, `certification`, `exist`) VALUES
(1, 'Người Dùng 1', 'nguoidung1@gmail.com', 'password1', '123456789', '1990-01-01', 'Nam', 'Người Dùng', 'Lập trình', 'Cert1', 1);
INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `skill`, `certification`, `exist`) VALUES
(2, 'Người Dùng 2', 'nguoidung2@gmail.com', 'password2', '987654321', '1995-05-05', 'Nữ', 'Người Dùng', 'Thiết kế', 'Cert2', 0);
INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `skill`, `certification`, `exist`) VALUES
(3, 'Người Dùng 3', 'nguoidung3@gmail.com', 'password3', '111222333', '1988-08-08', 'Nam', 'Người Dùng', 'Chăm sóc trẻ', 'Cert3', 0);
INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `skill`, `certification`, `exist`) VALUES
(4, 'Người Dùng 4', 'nguoidung4@gmail.com', 'password4', '444555666', '1992-12-12', 'Nữ', 'Người Dùng', 'Làm vườn', 'Cert4', 0),
(5, 'Người Dùng 5', 'nguoidung5@gmail.com', 'password5', '777888999', '1985-03-03', 'Nam', 'Người Dùng', 'Viết nội dung', 'Cert5', 0),
(6, 'abcd', 'abcd@gmail.com', 'abc', '6', '12/12/2023', 'Nữ', 'Người Dùng', '1', '2', 0),
(7, 'abc', 'abc@gmail.com', '$2b$10$ePSZOOQGhS1uOPvYgS.fqupMeG9DIxrT72q92G2kEN2OP/E.NQ.xW', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, '123', '123@gmail.com', '$2b$10$3zQP6RZn520iHBng7euPaum1/5ipaMIYTvEzh.Ao/4BESxhkRd61q', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, '1234', '1234@gmail.com', '$2b$10$.z6nkO8bBZmw1wXhOBzjFuhEuao4FV7dShMhiDY6Zdu7RUGjJFJxW', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 'bcd', 'bcd@gmail.com', 'bcd', '6', '12/12/2023', 'Nữ', 'Người Dùng', '1', '2', 0);

INSERT INTO `ThueCongViec` (`id`, `ma_cong_viec`, `ma_nguoi_thue`, `ngay_thue`, `hoan_thanh`, `exist`) VALUES
(1, 1, 3, '2023-01-05 14:30:00', 1, 0);
INSERT INTO `ThueCongViec` (`id`, `ma_cong_viec`, `ma_nguoi_thue`, `ngay_thue`, `hoan_thanh`, `exist`) VALUES
(2, 2, 4, '2023-02-10 16:45:00', 0, 0);
INSERT INTO `ThueCongViec` (`id`, `ma_cong_viec`, `ma_nguoi_thue`, `ngay_thue`, `hoan_thanh`, `exist`) VALUES
(3, 3, 5, '2023-03-15 18:30:00', 1, 0);
INSERT INTO `ThueCongViec` (`id`, `ma_cong_viec`, `ma_nguoi_thue`, `ngay_thue`, `hoan_thanh`, `exist`) VALUES
(4, 4, 1, '2023-04-20 20:15:00', 0, 0),
(5, 5, 2, '2023-05-25 22:00:00', 1, 0),
(7, 1, 7, '2023-05-25 22:00:00', 0, 0),
(12, 3, 1, '2024-11-23 17:00:00', 1, 1);


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;